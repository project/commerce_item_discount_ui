<?php

namespace Drupal\commerce_item_discount_ui\Plugin\Commerce\EntityTrait;

use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;

/**
 * Provides a trait to enable purchasing of subscriptions.
 *
 * @CommerceEntityTrait(
 *   id = "commerce_item_percentage_discount",
 *   label = @Translation("Allow percentage based discount"),
 *   entity_types = {"commerce_order_item"}
 * )
 */
class PercentageItemDiscountTrait extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    $fields['percentage_discount'] = BundleFieldDefinition::create('decimal')
      ->setLabel($this->t('Percentage discount'))
      ->setDescription($this->t('Percentage must be a positive number.'))
      ->setRequired(FALSE)
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 0)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 99,
        'settings' => [],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
