<?php

namespace Drupal\commerce_item_discount_ui\Plugin\Commerce\EntityTrait;

use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;

/**
 * Provides a trait to enable purchasing of subscriptions.
 *
 * @CommerceEntityTrait(
 *   id = "commerce_item_amount_discount",
 *   label = @Translation("Allow amount based discount"),
 *   entity_types = {"commerce_order_item"}
 * )
 */
class AmountItemDiscountTrait extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    $fields['amount_discount'] = BundleFieldDefinition::create('commerce_price')
      ->setLabel($this->t('Amount discount'))
      ->setDescription($this->t('Amount must be a positive number'))
      ->setCardinality(1)
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'commerce_unit_price',
        'weight' => 99,
        'settings' => [
          'require_confirmation' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
