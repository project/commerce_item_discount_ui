<?php

namespace Drupal\commerce_item_discount_ui\EventSubscriber;

use CommerceGuys\Intl\Calculator;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\commerce_order\Event\OrderItemEvent;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_tax\TaxOrderProcessor;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber for Commerce Orders.
 */
class OrderEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * Tax order processor.
   *
   * @var \Drupal\commerce_tax\TaxOrderProcessor
   */
  protected $taxOrderProcessor;

  /**
   * OrderEventSubscriber constructor.
   *
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   Price rounder.
   * @param \Drupal\commerce_tax\TaxOrderProcessor|null $taxOrderProcessor
   *   Tax order processor.
   */
  public function __construct(RounderInterface $rounder, TaxOrderProcessor $taxOrderProcessor = NULL) {
    $this->rounder = $rounder;
    $this->taxOrderProcessor = $taxOrderProcessor;
  }

  /**
   * Respond to event fired before saving and order item.
   */
  public function orderItemPresave(OrderItemEvent $event) {
    $entity = $event->getOrderItem();
    /** @var \Drupal\commerce_order\Entity\OrderItemTypeInterface $type */
    $type = $entity->type->entity;
    $rebuild = FALSE;
    if ($type->hasTrait('commerce_item_amount_discount') || $type->hasTrait('commerce_item_percentage_discount')) {
      $adjustments = $entity->getAdjustments(['promotion']);
      foreach ($adjustments as $adjustment) {
        if ($adjustment->getSourceId() || !$adjustment->isLocked()) {
          continue;
        }
        $entity->removeAdjustment($adjustment);
        $rebuild = TRUE;
      }
    }

    if ($type->hasTrait('commerce_item_amount_discount') && $entity->hasField('amount_discount') && $entity->amount_discount->first()) {
      /** @var \Drupal\commerce_price\Price $amount_discount */
      $amount_discount = $entity->amount_discount->first()->toPrice();
      $entity->addAdjustment(new Adjustment([
        'type' => 'promotion',
        'label' => $this->t('Order item discount'),
        'amount' => $amount_discount->multiply(-1),
        'locked' => TRUE,
      ]));
      $rebuild = TRUE;
    }

    if ($type->hasTrait('commerce_item_percentage_discount') && $entity->hasField('percentage_discount') && ($entity->percentage_discount->value > 0)) {
      /** @var \Drupal\commerce_price\Price $amount_discount */
      $percentage_discount = $entity->percentage_discount->value;
      if ($percentage_discount > 0) {
        $amount_discount = $entity->getTotalPrice()->multiply(Calculator::divide((string) $percentage_discount, 100));
        $amount_discount = $this->rounder->round($amount_discount);
        $entity->addAdjustment(new Adjustment([
          'type' => 'promotion',
          'label' => $this->t('Order item discount'),
          'amount' => $amount_discount->multiply(-1),
          'locked' => TRUE,
        ]));
        $rebuild = TRUE;
      }
    }

    // Calculate tax if tax module is available.
    if ($rebuild && ($this->taxOrderProcessor instanceOf TaxOrderProcessor) && $entity->getOrder() && $entity->getOrder()->getPlacedTime()) {
      // Remove existing VAT adjustments.
      foreach ($entity->getAdjustments(['tax']) as $adjustment) {
        $entity->removeAdjustment($adjustment);
      }
      // Create temp order.
      $temp_order = $entity->getOrder()->createDuplicate();
      // Add temp order line.
      $temp_item = $entity->createDuplicate();
      $temp_order->addItem($temp_item);
      // Run tax processor.
      $this->taxOrderProcessor->process($temp_order);
      // Copy calculated tax and set on $entity.
      foreach ($temp_item->getAdjustments(['tax']) as $adjustment) {
        $entity->addAdjustment($adjustment);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[OrderEvents::ORDER_ITEM_PRESAVE][] = ['orderItemPresave'];
    return $events;
  }

}
