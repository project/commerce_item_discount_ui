<?php

namespace Drupal\Tests\commerce_item_discount_ui\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_price\Price;
use Drupal\commerce_tax\Entity\TaxType;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Class TaxTest.
 *
 * @package Drupal\Tests\commerce_item_discount_ui\Kernel
 *
 * @group commerce_item_discount_ui
 */
class TaxTest extends OrderKernelTestBase {

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  public static $modules = [
    'commerce_tax',
    'commerce_tax_test',
    'commerce_item_discount_ui',
  ];

  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['commerce_tax']);
    $user = $this->createUser(['mail' => $this->randomString() . '@example.com']);

    $this->store->set('prices_include_tax', TRUE);
    $this->store->save();

    // The default store is US-WI, so imagine that the US has VAT.
    TaxType::create([
      'id' => 'us_vat',
      'label' => 'US VAT',
      'plugin' => 'custom',
      'configuration' => [
        'display_inclusive' => TRUE,
        'rates' => [
          [
            'id' => 'standard',
            'label' => 'Standard',
            'percentage' => '0.2',
          ],
        ],
        'territories' => [
          ['country_code' => 'US', 'administrative_area' => 'WI'],
          ['country_code' => 'US', 'administrative_area' => 'SC'],
        ],
      ],
    ])->save();

    $order = Order::create([
      'type' => 'default',
      'store_id' => $this->store->id(),
      'state' => 'draft',
      'mail' => $user->getEmail(),
      'uid' => $user->id(),
      'ip_address' => '127.0.0.1',
      'order_number' => '6',
    ]);
    $order->save();
    $this->order = $this->reloadEntity($order);


    /** @var \Drupal\commerce_order\Entity\OrderItemType $orderItemType */
    $orderItemType = OrderItemType::load('test');
    $orderItemType->setTraits(['commerce_item_amount_discount', 'commerce_item_percentage_discount']);
    $orderItemType->save();
    // Install the variation trait.
    /** @var \Drupal\commerce\EntityTraitManagerInterface $trait_manager */
    $trait_manager = \Drupal::service('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance('commerce_item_amount_discount');
    $trait_manager->installTrait($trait, 'commerce_order_item', 'test');
    $trait = $trait_manager->createInstance('commerce_item_percentage_discount');
    $trait_manager->installTrait($trait, 'commerce_order_item', 'test');
  }

  public function testTaxCalculation() {

    $order_item = OrderItem::create([
      'type' => 'test',
      'quantity' => '1',
      'unit_price' => new Price('12.00', 'USD'),
    ]);
    $order_item->save();
    $this->order->addItem($order_item);
    $this->order->save();

    // Confirm that the store address was used.
    $adjustments = $this->order->collectAdjustments();
    $adjustment = reset($adjustments);
    $this->assertCount(1, $adjustments);
    $this->assertEquals(new Price('2.00', 'USD'), $adjustment->getAmount());
    $this->assertEquals('us_vat|default|standard', $adjustment->getSourceId());

    // Add discount.
    $order_item->set('amount_discount', new Price('10.00', 'USD'));
    $order_item->save();
    $this->order->save();

    // Test promotion adjustment.
    $adjustments = $this->order->collectAdjustments(['promotion']);
    $adjustment = reset($adjustments);
    $this->assertCount(1, $adjustments);
    $this->assertEquals(new Price('-10', 'USD'), $adjustment->getAmount());

    // Confirm that the store address was used.
    $adjustments = $this->order->collectAdjustments(['tax']);
    $adjustment = reset($adjustments);
    $this->assertCount(1, $adjustments);
    $this->assertEquals(new Price('0.33', 'USD'), $adjustment->getAmount());
    $this->assertEquals('us_vat|default|standard', $adjustment->getSourceId());

  }

}
