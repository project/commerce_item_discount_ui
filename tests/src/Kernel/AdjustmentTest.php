<?php

namespace Drupal\Tests\commerce_item_discount_ui\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_price\Price;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Class AdjustmentTest
 *
 * @package Drupal\Tests\commerce_item_discount_ui\Kernel
 *
 * @group commerce_item_discount_ui
 */
class AdjustmentTest extends OrderKernelTestBase {

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce_item_discount_ui'
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $user = $this->createUser(['mail' => $this->randomString() . '@example.com']);
    $this->store->save();

    $order = Order::create([
      'type' => 'default',
      'store_id' => $this->store->id(),
      'state' => 'draft',
      'mail' => $user->getEmail(),
      'uid' => $user->id(),
      'ip_address' => '127.0.0.1',
      'order_number' => '6',
    ]);
    $order->save();
    $this->order = $this->reloadEntity($order);
    /** @var \Drupal\commerce_order\Entity\OrderItemType $orderItemType */
    $orderItemType = OrderItemType::load('test');
    $orderItemType->setTraits(['commerce_item_amount_discount', 'commerce_item_percentage_discount']);
    $orderItemType->save();
    // Install the variation trait.
    /** @var \Drupal\commerce\EntityTraitManagerInterface $trait_manager */
    $trait_manager = \Drupal::service('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance('commerce_item_amount_discount');
    $trait_manager->installTrait($trait, 'commerce_order_item', 'test');
    $trait = $trait_manager->createInstance('commerce_item_percentage_discount');
    $trait_manager->installTrait($trait, 'commerce_order_item', 'test');
  }

  /**
   * Tests whether promotion adjustments are correctly added.
   */
  public function testPromotionAdjustment() {
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = OrderItem::create([
      'type' => 'test',
      'quantity' => '1',
      'unit_price' => new Price('12.00', 'USD'),
    ]);
    $order_item->save();
    $this->order->addItem($order_item);
    $this->order->save();
    $this->assertEmpty($order_item->getAdjustments(['promotion']));
    $order_item->set('amount_discount', new Price('10.00', 'USD'));
    $order_item->save();
    $adjustments = $order_item->getAdjustments(['promotion']);
    $this->assertCount(1, $adjustments);
    $adjustment = reset($adjustments);
    $this->assertEquals($adjustment->getAmount()->getNumber(), '-10');
    $this->assertEquals($adjustment->getAmount()->getCurrencyCode(), 'USD');
  }

}
